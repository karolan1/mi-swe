https://yuml.me/diagram/nofunky/class/draw

%2F%2F Cool Class Diagram,
[Provozovatel|- IČO;-druh provozovatele;]
[Vlastník|- IČO]
[Vozidlo|-VIN;-celková hmotnost v kg;-obchodní označení;-tovární značka + obchodní označení + verze + varianta + typ vozidla]
[Kategorie vozidla]
[Barvy|-id;]
[Tovární značky]
[Kraje|-Kód Kraje (VÚSC);-Název Kraje (VÚSC)]
[Obce|-id obce;-název obce;]
[Typ obce]
[Technický průkaz|-číslo Technického průkazu;]
[Registrační místa|-název;]

[Registrace|-Počítačové číslo vozidla (PČV);-datum 1. registrace v ČR;-datum 1. registrace obecně (registrace např. i ve světě);-leasing;-č.j. spisu přestavby (je-li)]

[Okresy{bg:yellowgreen}|﻿-Kód;-Název Okresu;], 
[Okresy]<>výsledky okresu-[Výsledky 2. kola prez. voleb 2018|-hodnocení kandidáta č. 7 (Zeman);-hodnocení kandidáta č. 9 (Drahoš);-UCAST KOLO;-ZAPSANI_VOLICI;-VYDANE_OBALKY;-UCAST_PROC;-ODEVZDANE_OBALKY;-PLATNE_HLASY;-PLATNE_HLASY_PROC]

[Vozidlo]-je kategorie-[Kategorie vozidla],
[Vozidlo]-má hlavní barvu-[Barvy],
[Vozidlo]-má doplňkovou barvu (je-li)-[Barvy],
[Vozidlo]-je tovární značky-[Tovární značky],
[Vozidlo]-má vlastníka-[Vlastník],
[Vozidlo]-provozovatel-[Provozovatel],
[Registrace]-vozidla-[Vozidlo],
[Registrace]-okres registrace<>[Okresy],
[Registrace]-registrační místo-[Registrační místa];
[Okresy]-součást-[Kraje],
[Obce]-je typu-[Typ obce],
[Obce]-je v okresu-[Okresy],
[Technický průkaz]-[Vozidlo]