<rdf:RDF
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:qb="http://purl.org/linked-data/cube#"
xmlns:ex="http://karola.cz/swe/"
xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
xmlns:sdmx-dimension="http://purl.org/linked-data/sdmx/2009/dimension#"
xmlns:sdmx-attribute="http://purl.org/linked-data/sdmx/2009/attribute#"
xmlns:schema="http://schema.org/"
>

<qb:DataStructureDefinition rdf:about="http://karola.cz/swe/dsd-le">
    <qb:component>
      <rdf:Description>
        <qb:dimension rdf:resource="http://karola.cz/swe/refArea"/>
        <qb:order rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">1</qb:order>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:dimension rdf:resource="http://karola.cz/swe/attribute"/>
        <qb:order rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">2</qb:order>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/vysledekZeman"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/vysledekDrahos"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/zapsanychVolicu"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/vydanychObalek"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/odevzdanychObalek"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/platnychHlasu"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/ucast"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:measure rdf:resource="http://karola.cz/swe/platnychHlasuProc"/>
      </rdf:Description>
    </qb:component>

    <qb:component>
      <rdf:Description>
        <qb:attribute rdf:resource="http://purl.org/linked-data/sdmx/2009/attribute#unitMeasure"/>
        <qb:componentRequired rdf:datatype="https://www.w3.org/2001/XMLSchema#boolean">true</qb:componentRequired>
        <qb:componentAttachment rdf:resource="http://purl.org/linked-data/cube#DataSet"/>
      </rdf:Description>
    </qb:component>

  </qb:DataStructureDefinition>

<qb:DataSet rdf:about="http://karola.cz/swe/dataset-le1">
  <rdfs:label xml:lang="cs">Výsledky prezidentských voleb 2018</rdfs:label>
  <rdfs:comment xml:lang="cs">Výsledky prezidentských voleb 2018 v České republice, rozdělené podle okresů</rdfs:comment>
  <qb:structure rdf:resource="http://karola.cz/swe/dsd-le"/>
</qb:DataSet>

<rdf:Property rdf:about="http://karola.cz/swe/attribute">
    <rdf:type rdf:resource="http://purl.org/linked-data/cube#DimensionProperty"/>
    <rdfs:label xml:lang="cs">Měřený atribut</rdfs:label>
</rdf:Property>

<rdf:Property rdf:about="http://karola.cz/swe/refArea">
    <rdf:type rdf:resource="http://purl.org/linked-data/cube#DimensionProperty"/>
    <rdfs:label xml:lang="cs">Okres</rdfs:label>
</rdf:Property>
  {
    for $okres in //VYSLEDKY_KRAJ/KRAJ/OKRES
    return
      <rdf:Description rdf:about="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}">
        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:int"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/vysledekZeman"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:vysledekZeman rdf:datatype="xsd:int">{data($okres/CELKEM/HODN_KAND[@PORADOVE_CISLO="7"]/@HLASY)}</ex:vysledekZeman>
        </qb:Observation>
        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:int"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/vysledekDrahos"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:vysledekDrahos rdf:datatype="xsd:int">{data($okres/CELKEM/HODN_KAND[@PORADOVE_CISLO="9"]/@HLASY)}</ex:vysledekDrahos>
        </qb:Observation>

        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:int"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/zapsanychVolicu"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:zapsanychVolicu rdf:datatype="xsd:int">{data($okres/CELKEM/UCAST/@ZAPSANI_VOLICI)}</ex:zapsanychVolicu>
        </qb:Observation>
        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:int"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/vydanychObalek"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:vydanychObalek rdf:datatype="xsd:int">{data($okres/CELKEM/UCAST/@VYDANE_OBALKY)}</ex:vydanychObalek>
        </qb:Observation>
        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:int"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/odevzdanychObalek"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:odevzdanychObalek rdf:datatype="xsd:int">{data($okres/CELKEM/UCAST/@ODEVZDANE_OBALKY)}</ex:odevzdanychObalek>
        </qb:Observation>
        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:int"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/platnychHlasu"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:platnychHlasu rdf:datatype="xsd:int">{data($okres/CELKEM/UCAST/@PLATNE_HLASY)}</ex:platnychHlasu>
        </qb:Observation>

        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:decimal"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/ucast"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:ucast rdf:datatype="xsd:decimal">{data($okres/CELKEM/UCAST/@UCAST_PROC)}</ex:ucast>
        </qb:Observation>
        <qb:Observation rdf:about="http://karola.cz/swe/o1">
            <qb:dataSet rdf:resource="http://karola.cz/swe/dataset-le1"/>
            <sdmx-attribute:unitMeasure rdf:resource="xsd:decimal"/>
            <ex:attribute rdf:resource="http://karola.cz/swe/platnychHlasuProc"/>
            <ex:refArea rdf:resource="http://karola.cz/swe/okresy/{data($okres/@NUTS_OKRES)}"/>
            <ex:platnychHlasuProc rdf:datatype="xsd:decimal">{data($okres/CELKEM/UCAST/@PLATNE_HLASY_PROC)}</ex:platnychHlasuProc>
        </qb:Observation>

        <schema:name xml:lang="cs">
          {data($okres/@NAZ_OKRES)}
        </schema:name>
        <ex:idOkresu rdf:datatype="xsd:str">
          {data($okres/@NUTS_OKRES)}
        </ex:idOkresu>
      </rdf:Description>
  }
</rdf:RDF>
