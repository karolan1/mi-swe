Seznam krajů ČR
http://vdp.cuzk.cz/vdp/ruian/okresy/vyhledej?vc.kod=&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=NAZEV&search=Vyhledat
(http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=NAZEV&export=CSV)



Volební výsledky 2. kola volby prezidenta republiky leden 2018
z https://volby.cz/opendata/prez2018/prez2018_opendata.htm

Jednotlivá URL: https://volby.cz/pls/prez2018/vysledky_kraj?kolo=2&nuts=CZXXX, kde CZXXX je kód kraje dle https://volby.cz/opendata/prez2018/PREZ_nuts.xls



Měsíční statistiky vyplývající z Centrálního registru vozidel - leden 2018
https://www.mdcr.cz/Statistiky/Silnicni-doprava/Centralni-registr-vozidel/Mesicni-statistiky-2018?returl=/Statistiky/Silnicni-doprava/Centralni-registr-vozidel

(https://www.mdcr.cz/getattachment/Statistiky/Silnicni-doprava/Centralni-registr-vozidel/Mesicni-statistiky-2018/REG1801.zip.aspx?lang=cs-CZ)