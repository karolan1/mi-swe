import org.apache.jena.query.*;
import org.apache.jena.rdf.model.RDFNode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {

    public static final Query QUERY = QueryFactory.create("# select registered cars with weight over 10 tons\n" +
            "\n" +
            "PREFIX schema: <http://schema.org/>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX gr: <http://purl.org/goodrelations/v1#>\n" +
            "\n" +
            "SELECT ?a ?desc ?w ?model\n" +
            "WHERE {\n" +
            "  ?a rdf:type schema:Car;\n" +
            "     schema:weightTotal ?blank.\n" +
            "  OPTIONAL {\n" +
            "    ?a schema:model ?model.\n" +
            "  }\n" +
            "  ?a schema:description ?desc.\n" +
            "  ?blank gr:hasValue ?w.\n" +
            "  FILTER (?w > 10000)\n" +
            "}" +
            "LIMIT 100");

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QueryExecution qexec = QueryExecutionFactory
                .createServiceRequest("http://localhost:3030/combined", QUERY);
        try {
            ResultSet results = qexec.execSelect();
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution() ;
                RDFNode x = soln.get("desc") ; // Get a result variable by name.
                response.getWriter().println(x.toString());
            }
        } finally {
            qexec.close() ;
        }
    }
}
